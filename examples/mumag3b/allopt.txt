#########################################################################
# magpar configuration file: allopt.txt
#########################################################################

-simName mumag3

-refine 3

-size 30e-9

-init_mag 1

-mode 1

-slice_n 0,0,1
-slice_p 0,0,0

-line_v 0,0,1
-line_p 0,0,0

-logpid 1

-jfinal -0.95

-tao_fatol 1e-8
-tao_frtol 0.0
