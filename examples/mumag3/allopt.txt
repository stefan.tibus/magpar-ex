#########################################################################
# magpar configuration file: allopt.txt
#########################################################################

-simName mumag3

-meshtype 0         # read finite element mesh from Patran neutral file

-refine 4           # refine mesh 4 times

-size 30e-9         # set length scaling factor to 30 nm

-init_mag 1         # initialize magnetization parallel to x-axis

-mode 1             # select energy minimization using TAO

-slice_n 0,0,1      # slice plane for png files: x-y-plane
-slice_p 0,0,0

-line_v 0,0,1       # set measurement line parallel to z-axis
-line_p 0,0,0

-tao_fatol 1e-8     # set absolute tolerance for energy minimizer TAO
-tao_frtol 0.0      # set relative tolerance for energy minimizer TAO
