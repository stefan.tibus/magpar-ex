#########################################################################
# magpar configuration file: allopt.txt
#########################################################################

-simName sphere

-meshtype 0         # read finite element mesh from Patran neutral file

-init_mag 6         # set magnetization in x-z plane to...
-init_magparm 0.785 # theta = 0.785 rad = 45 deg (from z-axis)

-demag 0            # switch off demagnetizing field
