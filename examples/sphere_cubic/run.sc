#!/usr/local/bin/bash

# set number of processors unless defined by an environment variable
if [ x$np = "x" ]; then
  np=1
fi
prg=$PWD/magpar.O_c++

echo "- Start -----"
hostname
date
echo "processors: $np"
echo ""

nmax=100
for (( i=0 ; $i <= $nmax ; i=$i+1 )) ; do
  theta=`echo "$i*3.14159265/$nmax" | bc -l`
  echo "# --- theta: $theta" >> sphere.log
#  prun -p scp1 -n 1 -s -o xout -e xerr ./magpar.O_c++ -init_magparm $theta $@
  mpirun -np $np $prg -init_magparm $theta $@
done

