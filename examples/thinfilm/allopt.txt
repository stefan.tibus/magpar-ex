#########################################################################
# magpar configuration file: allopt.txt
#########################################################################

-simName thinfilm

-size 1e-6          # set length scaling factor to microns

-slice_n 0,0,1      # slice plane for png files: x-y-plane
-slice_p 0,0,0.025
