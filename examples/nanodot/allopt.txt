#########################################################################
# magpar configuration file: allopt.txt
#########################################################################

-simName nanodot

-size 100e-9        # set length scaling factor to 100 nm

-init_mag 0         # read magnetization from inp file with...

-inp 0059           # ...inp number 0059

-mode 0             # select LLG time integration (PVode solver)

-hextini 32         # apply a homogeneous external field
-htheta  1.5707963  # apply homogeneous external field parallel to x-axis
-hstep   -5         # change field amplitude in steps of -5 kA/m
-hfinal  -200       # final field value -200 kA/m

-condinp_j  0.05    # save output files when J//Hext changes by 0.05
-condinp_t  1e99    # do not save output files at regular time intervals

-slice_n 0,0,1      # slice plane for png files: x-y-plane
-slice_p 0,0,0.1

-jfinal -0.2        # stop simulation if |J//Hext| < jfinal

-ts_max_time 1e99   # effectively disable maximum simulation time
